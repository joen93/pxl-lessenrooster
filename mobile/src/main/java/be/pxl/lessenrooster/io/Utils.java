package be.pxl.lessenrooster.io;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.io.model.Les;
import be.pxl.lessenrooster.io.model.Vak;


/**
 * Created by jeroen on 19/04/15.
 */
public class Utils {
    private static final String PREFS_FILE_NAME = "LessenroosterPrefs";
    private static final String PREFS_CHECKED_VAKKEN = "checkedVakken";
    private static final int DEFAULT_AMOUNT_OF_DAYS_TO_SHOW = 14;
    private static final int DEFAULT_AMOUNT_OF_PAST_WEEKS_TO_SHOW = 0;
    private static final String PREFS_AMOUNT_OF_DAYS_TO_SHOW = "pref_amountOfDays";
    private static final String PREFS_AMOUNT_OF_PAST_WEEKS_TO_SHOW = "pref_amountOfWeeksPast";
    private static final String LESSENROOSTER_FILE_NAME = "lessenrooster";
    private static final String KLASSEN_FILE_NAME = "klassen";
    private static final String PREFS_LESSENROOSTER_PERIOD = "period";
    private static final String PREFS_UPDATED_VAKKEN = "updatedVakken";
    private Context mContext;
    private IAsyncResponse<String> mCallbacks;
    private SharedPreferences mPreferences;
    private SharedPreferences mDefPreferences;
    private ArrayList<ArrayList<Les>> mDays;
    private ArrayList<String> mDates;
    private int mDaysAmount = 0;
    private int mWeeksPastAmount = 0;
    private ProgressBar mProgressSpinner;
    private MenuItem mActionRefresh;
    /**
     * flag to help showing the {@link Utils#mProgressSpinner} even when {@link Utils#mActionRefresh} updates
     */
    private boolean mIsRefreshing = false;

    public Utils(Context mContext) {
        this.mContext = mContext;
        mPreferences = getContext().getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);
        mDefPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    /**
     * Method to check if the user is online and has access to internet (not just network)
     *
     * @param asyncResponse callback object
     */
    public static void isOnline(final int timeout, final IAsyncResponse<Boolean> asyncResponse) {
        new AsyncTask<Void, Void, Boolean>() {
            private Runtime mRuntime;

            @Override
            protected void onPreExecute() {
                //runtime so we can run our bash ping command
                mRuntime = Runtime.getRuntime();
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                Integer exitValue = null;
                try {
                    String command = "/system/bin/ping -c 1 -W " + timeout + " data.pxl.be";
                    // send 1 ping to the server and wait 3 sec for a reply
                    Process ipProcess = mRuntime.exec(command);
                    exitValue = ipProcess.waitFor(); //wait for a result (result doesn't mean there is internet)
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
                //ping response 0 means we've got a response from the host -> true
                return ((exitValue != null && exitValue == 0));
            }

            @Override
            protected void onPostExecute(Boolean bool) {
                asyncResponse.processFinish(bool);
            }
        }.execute();
    }

    public static ArrayList<?> jsonToList(TypeToken typeToken, String json) {
        ArrayList<?> list = new ArrayList<>();
        Gson gson = new Gson();

        try {
            list = gson.fromJson(json, typeToken.getType());
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return list;
    }

    private Context getContext() {
        return mContext;
    }

    public void setCallbacks(IAsyncResponse<String> callbacks) {
        this.mCallbacks = callbacks;
    }

    public void getKlassen() {
        AsyncCacheReader reader = new AsyncCacheReader(getContext(), new IAsyncResponse<String>() {
            @Override
            public void processFinish(String output) {
                if (!output.isEmpty()) {
                    mCallbacks.processFinish(output);
                } else {
                    getKlassenFromWeb();
                }
            }
        });
        reader.execute(KLASSEN_FILE_NAME);
    }

    private void getKlassenFromWeb() {
        final AsyncHttpTask httpTask = new AsyncHttpTask(getContext(), true, new IAsyncResponse<String>() {
            @Override
            public void processFinish(String output) {
                if (!output.isEmpty()) {
                    AsyncCacheWriter writer = new AsyncCacheWriter(getContext());
                    writer.execute(KLASSEN_FILE_NAME, output);

                    mCallbacks.processFinish(output);
                }
            }
        });

        isOnline(3, new IAsyncResponse<Boolean>() {
            @Override
            public void processFinish(Boolean output) {
                if (output)
                    httpTask.execute("klassen");
                else {
                    Toast.makeText(mContext, R.string.toast_offline, Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void getDays(boolean forceSync) {
        //if there are changed checked vakken or the cache outdated, get new data from web
        if (forceSync || changedVakken() || isCacheOutdated()) {
            //get the shared preference containing the checked vakken

            String checkedVakkenJson = mPreferences.getString(PREFS_CHECKED_VAKKEN, "");
            //get the desired  amount of days from the preference
            if (mDaysAmount == 0)
                mDaysAmount = mDefPreferences.getInt(PREFS_AMOUNT_OF_DAYS_TO_SHOW, DEFAULT_AMOUNT_OF_DAYS_TO_SHOW);
            if (mWeeksPastAmount == 0)
                mWeeksPastAmount = mDefPreferences.getInt(PREFS_AMOUNT_OF_PAST_WEEKS_TO_SHOW, DEFAULT_AMOUNT_OF_PAST_WEEKS_TO_SHOW);

            int totalDays = mDaysAmount + (7 * mWeeksPastAmount);

            //if there are checked vakken
            if (checkedVakkenJson != null && !checkedVakkenJson.isEmpty()) {
                mDays = new ArrayList<>();
                for (int i = 0; i < totalDays; i++) {
                    mDays.add(new ArrayList<Les>());
                }
                mDates = getDaysOfWeek(); //the period of which we want data to be shown

                TypeToken typeToken = new TypeToken<ArrayList<Vak>>() {
                };
                //list of checked vakken
                final ArrayList<Vak> vakken = (ArrayList<Vak>) jsonToList(typeToken, checkedVakkenJson);

                final int lastIndex = vakken.size() - 1;
                //check if there's an internet connection before getting the data
                isOnline(3, new IAsyncResponse<Boolean>() {
                    @Override
                    public void processFinish(Boolean output) {
                        toggleRefreshing(output);
                        if (output) {
                            for (Vak vak : vakken) {
                                if (vakken.indexOf(vak) == lastIndex)
                                    getLessenFromWeb(vak.buildApiPath(), true);
                                else
                                    getLessenFromWeb(vak.buildApiPath(), false);
                            }
                        } else
                            Toast.makeText(mContext, R.string.toast_offline, Toast.LENGTH_LONG).show();
                    }
                });
            }
        } else {
            AsyncCacheReader reader = new AsyncCacheReader(getContext(), new IAsyncResponse<String>() {
                @Override
                public void processFinish(String output) {
                    mCallbacks.processFinish(output);
                }
            });
            reader.execute(LESSENROOSTER_FILE_NAME);
        }
    }

    /**
     * @param vakPath api path of the vak
     * @param last    last vak from the "vakken" list
     */
    private void getLessenFromWeb(final String vakPath, final boolean last) {

        final AsyncHttpTask httpTask = new AsyncHttpTask(getContext(), false, new IAsyncResponse<String>() {
            @Override
            public void processFinish(String json) {
                if (!json.isEmpty()) {
                    TypeToken typeToken = new TypeToken<ArrayList<Les>>() {
                    };
                    ArrayList<Les> lessen = (ArrayList<Les>) jsonToList(typeToken, json);//lessen from multiple days
                    orderLessenInDays(lessen);
                    if (last)
                        postGetLessenFromWeb();
                }
            }
        });

        isOnline(3, new IAsyncResponse<Boolean>() {
            @Override
            public void processFinish(Boolean output) {
                if (output)
                    httpTask.execute(vakPath);
                else {
                    String vak = vakPath.replace("klassen/", "");
                    vak = vak.replace("/olod/", " ");
                    Toast.makeText(mContext, "Kon '" + vak + "' niet ophalen. Probeer opnieuw", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void postGetLessenFromWeb() {
        if (!mDays.isEmpty()) {
            for (int i = 0; i < mDays.size(); i++) {
                ArrayList<Les> day = mDays.get(i);
                Collections.sort(day);
                mDays.set(i, day);
            }
            String daysJson = new Gson().toJson(mDays);

            mCallbacks.processFinish(daysJson);
            toggleRefreshing(false);

            //cache mDays (lessenrooster/weekrooster)
            AsyncCacheWriter writer = new AsyncCacheWriter(getContext());
            writer.execute(LESSENROOSTER_FILE_NAME, daysJson);

            Intent intent = new Intent(mContext, WearSyncService.class);
            intent.putExtra(Constants.DAYS_PATH, daysJson);
            intent.putExtra(Constants.DATES_PATH, mPreferences.getString(PREFS_LESSENROOSTER_PERIOD, ""));
            mContext.startService(intent);

            SharedPreferences.Editor editor = mPreferences.edit();
            editor.putBoolean(PREFS_UPDATED_VAKKEN, false).apply();
        }
    }

    private void orderLessenInDays(ArrayList<Les> lessen) {
        ArrayList<Les> day;
        for (Les les : lessen) {
            String date = les.getDatum();
            if (mDates.contains(date)) {
                int i = mDates.indexOf(date);
                day = mDays.get(i);

                if (!day.contains(les)) {
                    day.add(les);
                    mDays.set(i, day);
                }
            }
        }
    }

    //TODO optimize: cache days to improve performance by eliminating the need recalculating the days every time
    private ArrayList<String> getDaysOfWeek() {
        Calendar now = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");

        if (mDaysAmount == 0)
            mDaysAmount = mDefPreferences.getInt(PREFS_AMOUNT_OF_DAYS_TO_SHOW, DEFAULT_AMOUNT_OF_DAYS_TO_SHOW);
        if (mWeeksPastAmount == 0)
            mWeeksPastAmount = mDefPreferences.getInt(PREFS_AMOUNT_OF_PAST_WEEKS_TO_SHOW, DEFAULT_AMOUNT_OF_PAST_WEEKS_TO_SHOW);

        ArrayList<String> days = new ArrayList<>(mDaysAmount);

        //the desired past weeks in days
        int delta = 7 * mWeeksPastAmount;
        //TODO implement desired start day of week here (sunday, monday, ... etc.)
        //subtract "now" with the desired past days, to change "now" to a desired point in the past
        now.add(Calendar.DAY_OF_MONTH, -delta); //now = today minus X weeks ago

        //for each day in the future and past: add its date to the days arrays
        for (int i = 0; i < (mDaysAmount + delta); i++) {
            days.add(sdf.format(now.getTime()));
            now.add(Calendar.DAY_OF_MONTH, 1); //next day
        }

        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PREFS_LESSENROOSTER_PERIOD, new Gson().toJson(days));
        editor.apply();
        return days;
    }

    private boolean isCacheOutdated() {

        String cachedJson = mPreferences.getString(PREFS_LESSENROOSTER_PERIOD, "");
        mDaysAmount = mDefPreferences.getInt(PREFS_AMOUNT_OF_DAYS_TO_SHOW, DEFAULT_AMOUNT_OF_DAYS_TO_SHOW);
        mWeeksPastAmount = mDefPreferences.getInt(PREFS_AMOUNT_OF_PAST_WEEKS_TO_SHOW, DEFAULT_AMOUNT_OF_PAST_WEEKS_TO_SHOW);

        if (cachedJson != null && !cachedJson.isEmpty()) {

            try {
                Type listType = new TypeToken<ArrayList<String>>() {
                }.getType();
                ArrayList<String> cachedDays = new Gson().fromJson(cachedJson, listType);

                //if the size of our cache is different from the total days of the user preferences, it's 'outdated'
                if (cachedDays.size() != (mDaysAmount + (mWeeksPastAmount * 7)))
                    return true;

                Calendar now = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
                String nowStr = sdf.format(now.getTime());

                //if the current date is in cached array of dates, then it's not outdated
                if (cachedDays.contains(nowStr))
                    return false;

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private boolean changedVakken() {
        return mPreferences.getBoolean(PREFS_UPDATED_VAKKEN, false);
    }

    public ArrayList<String> getDates() {
        if (mDates != null) {
            return mDates;
        }
        String json = mPreferences.getString(PREFS_LESSENROOSTER_PERIOD, "");
        if (json != null && json.isEmpty()) {
            TypeToken typeToken = new TypeToken<ArrayList<String>>() {
            };
            mDates = (ArrayList<String>) Utils.jsonToList(typeToken, json);
            return mDates;
        }
        //mdates is null and our cache is empty
        mDates = this.getDaysOfWeek();
        return mDates;
    }

    public void setProgressSpinner(ProgressBar progressSpinner) {
        this.mProgressSpinner = progressSpinner;
    }

    public void setActionRefresh(MenuItem actionRefresh) {
        this.mActionRefresh = actionRefresh;
        toggleRefreshing(mIsRefreshing);
    }

    private void toggleRefreshing(boolean bool) {
        if (bool) {
            mActionRefresh.setActionView(mProgressSpinner);
            mIsRefreshing = true;
        } else {
            mActionRefresh.setActionView(null);
            mIsRefreshing = false;
        }
    }
}
