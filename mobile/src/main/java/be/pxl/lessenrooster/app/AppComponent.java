package be.pxl.lessenrooster.app;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by jeroendebois on 07/08/16.
 */
@Singleton
@Component(modules = {AppModule.class, GoogleModule.class})
public interface AppComponent extends AppContextComponent {
    /**
     * An initializer that creates the graph from an application.
     */
    final static class Initializer {
        static AppContextComponent init(PxlApplication app) {
            return DaggerAppComponent.builder()
                    .appModule(new AppModule(app))
                    .build();
        }

        private Initializer() {
        } // No instances.
    }
}
