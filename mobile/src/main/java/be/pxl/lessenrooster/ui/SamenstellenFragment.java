package be.pxl.lessenrooster.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashSet;

import javax.inject.Inject;

import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.app.PxlApplication;
import be.pxl.lessenrooster.constants.Tracking;
import be.pxl.lessenrooster.io.AsyncHttpTask;
import be.pxl.lessenrooster.io.IAsyncResponse;
import be.pxl.lessenrooster.io.KlassenSpinAdapter;
import be.pxl.lessenrooster.io.Utils;
import be.pxl.lessenrooster.io.VakkenAdapter;
import be.pxl.lessenrooster.io.VakkenCallbacks;
import be.pxl.lessenrooster.io.model.Klas;
import be.pxl.lessenrooster.io.model.Vak;
import be.pxl.lessenrooster.manager.TrackingManager;

public class SamenstellenFragment extends Fragment implements IAsyncResponse<String> {
    private static final String TAG = "SamenstellenFragment";
    private static final String PREFS_FILE_NAME = "LessenroosterPrefs";
    private static final String PREFS_CHECKED_VAKKEN = "checkedVakken";
    private static final String PREFS_UPDATED_VAKKEN = "updatedVakken";

    private View mRootView;
    private Spinner mKlassenSpinner;
    private KlassenSpinAdapter mSpinAdapter;
    private RecyclerView mVakkenRecyclerView;
    private VakkenAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Klas> mKlassen;
    private CheckHandler mHandler;
    private String mCachedJson;
    private Utils mUtils;
    @Inject
    TrackingManager mTrackingManager;

    public SamenstellenFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PxlApplication.get(getContext()).component().inject(this);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_samenstellen, container, false);
        mRootView.setTag(TAG);

        mKlassen = new ArrayList<>();

        this.mUtils = new Utils(getActivity());
        mUtils.setCallbacks(this);
        mUtils.getKlassen();

        return mRootView;
    }

    private void init() {
        mKlassenSpinner = (Spinner) mRootView.findViewById(R.id.klassen_spinner);
        mSpinAdapter = new KlassenSpinAdapter(getActivity(), R.layout.spinner_item_klas);
        mKlassenSpinner.setAdapter(mSpinAdapter);
        mKlassenSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String name = mKlassen.get(position).getName();
                //TODO move over to utils for cleaner code
                AsyncHttpTask httpTask = new AsyncHttpTask(getActivity(), true, new IAsyncResponse<String>() {
                    @Override
                    public void processFinish(String json) {
                        if (!json.isEmpty()) {

                            TypeToken typeToken = new TypeToken<ArrayList<Vak>>() {
                            };
                            Gson gson = new Gson();

                            try {
                                ArrayList<Vak> list = gson.fromJson(json, typeToken.getType());
                                mAdapter.setVakken(list);
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                httpTask.execute("klassen/" + name + "/vakken");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mVakkenRecyclerView = (RecyclerView) mRootView.findViewById(R.id.vakken_recycler);
        Context context = getActivity();
        mLayoutManager = new LinearLayoutManager(context);
        mVakkenRecyclerView.setLayoutManager(mLayoutManager);
        mCachedJson = readPrefs();
        mHandler = new CheckHandler(mCachedJson);
        mAdapter = new VakkenAdapter(initDummyVakken(0), mHandler);
        mVakkenRecyclerView.setAdapter(mAdapter);

        if (mKlassen.isEmpty()) {
            ((MainActivity) getActivity()).mNavigationDrawerFragment.onNavigationDrawerItemSelected(0);
            Toast.makeText(context, R.string.server_error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mTrackingManager.trackScreen(Tracking.Screens.SAMENSTELLEN);
    }

    @Override
    public void onDestroyView() {
        if (mHandler != null) {
            String json = mHandler.toJson();
            if (!mCachedJson.equals(json)) {
                writePrefs(json);
                analytics();
            }
        }
        super.onDestroyView();
    }

    private ArrayList<Vak> initDummyVakken(int i) {
        ArrayList<Vak> vakken = new ArrayList<>();
        Klas klas = mKlassen.isEmpty() ? new Klas("Geen klassen beschikbaar", "1337") : mKlassen.get(i);
        for (int j = 0; j <= 10; j++) {
            vakken.add(new Vak(klas.getName(), "code" + j, "les" + j, "docent" + j));
        }
        return vakken;
    }

    private String readPrefs() {
        SharedPreferences preferences = getActivity().getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(PREFS_CHECKED_VAKKEN, "");
    }

    private void writePrefs(String json) {
        SharedPreferences preferences = getActivity().getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREFS_CHECKED_VAKKEN, json);
//        if (!mCachedJson.equals(json))
        editor.putBoolean(PREFS_UPDATED_VAKKEN, true);
        editor.apply();
    }

    @Override
    public void processFinish(String output) {
        TypeToken typeToken = new TypeToken<ArrayList<Klas>>() {
        };
        mKlassen = (ArrayList<Klas>) Utils.jsonToList(typeToken, output);
        init();
        mSpinAdapter.setKlassen(mKlassen);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.samenstellen, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear:
                Toast.makeText(getActivity(), R.string.cleared_selections, Toast.LENGTH_SHORT).show();
                mHandler.clearAllChecks();
                mAdapter.clearChecks();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method to check in the background what changed
     * and send these eventual new classes and/or courses to analytics
     */
    private void analytics() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                ArrayList<Vak> cachedVakken = (ArrayList<Vak>) mUtils.jsonToList(new TypeToken<ArrayList<Vak>>() {
                }, mCachedJson);
                //because cachedVakken is null on the first run
                cachedVakken = cachedVakken == null ? new ArrayList<Vak>() : cachedVakken;

                HashSet<String> chachedKlassenSet = new HashSet<>();
                for (Vak vak : cachedVakken) {
                    chachedKlassenSet.add(vak.getKlas());
                }

                HashSet<String> chachedOlodsSet = new HashSet<>();
                for (Vak vak : cachedVakken) {
                    chachedOlodsSet.add(vak.getOlod());
                }

                ArrayList<String> newKlassen = mHandler.getSelectedKlassen();
                for (String klas : chachedKlassenSet) {
                    newKlassen.remove(klas);
                }

                ArrayList<String> newOlods = mHandler.getSelectedOlods();
                for (String olod : chachedOlodsSet) {
                    newOlods.remove(olod);
                }

                for (String klasnaam : newKlassen) {
                    mTrackingManager.trackEvent(Tracking.Categories.PERSONAL_ROSTER,
                            Tracking.Actions.ADD_CLASS,
                            klasnaam);
                }

                for (String olodnaam : newOlods) {
                    mTrackingManager.trackEvent(Tracking.Categories.PERSONAL_ROSTER,
                            Tracking.Actions.ADD_OLOD,
                            olodnaam);
                }

                return null;
            }
        }.execute();

    }


    private class CheckHandler implements VakkenCallbacks {
        private ArrayList<Vak> mCheckedVakken;

        private CheckHandler(String json) {
            mCheckedVakken = fromJson(json);
        }

        @Override
        public void onItemSelected(Vak vak, boolean checked) {
            if (checked)
                checkVak(vak);
            else
                uncheckVak(vak);
        }

        private void checkVak(Vak vak) {
            if (!containsVak(vak))
                this.mCheckedVakken.add(vak);
        }

        private void uncheckVak(Vak vak) {
            if (containsVak(vak))
                this.mCheckedVakken.remove(vak);
        }

        public void clearAllChecks() {
            this.mCheckedVakken.clear();
        }

        @Override
        public boolean containsVak(Vak vak) {
            for (Vak vak1 : mCheckedVakken) {
                if (vak1.equals(vak))
                    return true;
            }
            return false;
        }

        public String toJson() {
            String json = "";
            if (!mCheckedVakken.isEmpty()) {
                Gson gson = new Gson();
                json = gson.toJson(mCheckedVakken);
            }
            return json;
        }

        public ArrayList<Vak> fromJson(String json) {
            ArrayList<Vak> list = new ArrayList<>();

            if (!json.isEmpty()) {
                Gson gson = new Gson();
                TypeToken typeToken = new TypeToken<ArrayList<Vak>>() {
                };

                try {
                    list = gson.fromJson(json, typeToken.getType());
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
            return list;
        }

        public ArrayList<String> getSelectedKlassen() {
            HashSet<String> list = new HashSet<>();
            for (Vak vak : mCheckedVakken) {
                list.add(vak.getKlas());
            }
            return new ArrayList<>(list);
        }

        public ArrayList<String> getSelectedOlods() {
            HashSet<String> list = new HashSet<>();
            for (Vak vak : mCheckedVakken) {
                list.add(vak.getOlod());
            }
            return new ArrayList<>(list);
        }
    }
}
