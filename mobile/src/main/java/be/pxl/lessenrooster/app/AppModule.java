package be.pxl.lessenrooster.app;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jeroendebois on 07/08/16.
 */
@Module
public class AppModule {

    private final PxlApplication app;

    public AppModule(final PxlApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return app;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return app;
    }

}
