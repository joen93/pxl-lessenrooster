package be.pxl.lessenrooster.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.io.LessenAdapter;
import be.pxl.lessenrooster.io.model.Les;

public class LessenroosterFragment extends Fragment {
    private static final String TAG = "LessenroosterFragment";

    private RecyclerView mLessenRecyclerView;
    private LessenAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Les> mLessen;

    public LessenroosterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mLessen = this.getArguments().getParcelableArrayList("lessen");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_lessenrooster, container, false);
        rootView.setTag(TAG);

        mLessenRecyclerView = (RecyclerView) rootView.findViewById(R.id.lessen_recycler);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mLessenRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new LessenAdapter(mLessen);
        mLessenRecyclerView.setAdapter(mAdapter);

        return rootView;
    }

}
