package be.pxl.lessenrooster.manager.impl;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import be.pxl.lessenrooster.manager.TrackingManager;

/**
 * Created by jeroendebois on 07/08/16.
 */
public class GoogleTrackingManager implements TrackingManager {

    private final Tracker mTracker;

    public GoogleTrackingManager(Tracker tracker) {
        mTracker = tracker;
    }

    @Override
    public void trackScreen(String screen) {
        mTracker.setScreenName(screen);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void trackEvent(String category, String action, String label) {
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder().setCategory(category).setAction(action);
        if (label != null) {
            builder.setLabel(label);
        }
        mTracker.send(builder.build());
    }
}
