package be.pxl.lessenrooster.io;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by jeroen on 19/04/15.
 */
public class Utils {
    private Context mContext;
    private AsyncResponse mCallbacks;
    private SharedPreferences mPreferences;

    public Utils(Context mContext) {
        this.mContext = mContext;
        mPreferences = getContext().getSharedPreferences(Constants.PREFS_FILE_NAME, Context.MODE_PRIVATE);
    }


    public static ArrayList<?> jsonToList(TypeToken typeToken, String json) {
        ArrayList<?> list = new ArrayList<>();
        Gson gson = new Gson();

        try {
            list = gson.fromJson(json, typeToken.getType());
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return list;
    }

    private Context getContext() {
        return mContext;
    }

    public void setCallbacks(AsyncResponse callbacks) {
        this.mCallbacks = callbacks;
    }


    public void getDays() {
        AsyncCacheReader reader = new AsyncCacheReader(getContext(), new AsyncResponse() {
            @Override
            public void processFinish(String output) {
                mCallbacks.processFinish(output);
            }
        });
        reader.execute(Constants.DAYS_PATH);

    }

    public ArrayList<String> getDates() {
        ArrayList<String> dates;
        String json = mPreferences.getString(Constants.PREFS_LESSENROOSTER_PERIOD, "");

        if (json != null && json.isEmpty()) {
            String[] array = new String[14];
            for (int i = 0; i < 14; i++)
                array[i] = "geen data";
            dates = new ArrayList<>(Arrays.asList(array));
        } else {
            TypeToken typeToken = new TypeToken<ArrayList<String>>() {
            };
            dates = (ArrayList<String>) Utils.jsonToList(typeToken, json);
        }
        return dates;
    }
}
