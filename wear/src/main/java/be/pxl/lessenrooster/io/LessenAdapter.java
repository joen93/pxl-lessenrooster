package be.pxl.lessenrooster.io;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.io.model.Les;

/**
 * Created by jeroen on 14/04/15.
 */
public class LessenAdapter extends RecyclerView.Adapter<LessenAdapter.ViewHolder> {
    private ArrayList<Les> mLessen;
    private SimpleDateFormat sdfIn = new SimpleDateFormat("HHmm");
    private SimpleDateFormat sdfOut = new SimpleDateFormat("HH:mm");


    public LessenAdapter(ArrayList<Les> lessen) {
        this.mLessen = lessen;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item_les, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.mStartTime.setText(formatTime(mLessen.get(i).getStartTime()));
        viewHolder.mEndTime.setText(formatTime(mLessen.get(i).getEndTime()));
        if (mLessen.get(i).getConflict()) {
            viewHolder.mConflict.setVisibility(View.VISIBLE);
        }

        viewHolder.mName.setText(mLessen.get(i).getName());
        viewHolder.mDocent.setText(mLessen.get(i).getDocent());
        viewHolder.mLokaal.setText(mLessen.get(i).getLokaal());
    }

    @Override
    public int getItemCount() {
        return mLessen != null ? mLessen.size() : 0;
    }

    private String formatTime(String time) {
        if (time.length() == 3)
            time = "0" + time;
        try {
            Date date = sdfIn.parse(time);
            return sdfOut.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return time;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mStartTime;
        private TextView mEndTime;
        private TextView mConflict;

        private TextView mName;
        private TextView mDocent;
        private TextView mLokaal;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mStartTime = (TextView) itemView.findViewById(R.id.start_time);
            this.mEndTime = (TextView) itemView.findViewById(R.id.end_time);
            this.mConflict = (TextView) itemView.findViewById(R.id.conflict_warning);
            this.mName = (TextView) itemView.findViewById(R.id.les_name);
            this.mDocent = (TextView) itemView.findViewById(R.id.les_docent);
            this.mLokaal = (TextView) itemView.findViewById(R.id.les_lokaal);
        }
    }
}

