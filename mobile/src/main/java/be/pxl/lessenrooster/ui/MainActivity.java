package be.pxl.lessenrooster.ui;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;

import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.io.NavigationDrawerCallbacks;

//todo transitions
//todo translations
//todo app invites? https://developers.google.com/app-invites/
public class MainActivity extends AppCompatActivity
        implements NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    protected NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        // populate the navigation drawer
        mNavigationDrawerFragment.setUserData("PXL", "Lessenrooster", BitmapFactory.decodeResource(getResources(), R.drawable.logo));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        switch (position) {
            case 0:
                replaceFragment(new WeekroosterFragment());
                break;
            case 1:
                replaceFragment(new SamenstellenFragment());
                break;
            case 2:
                replaceFragment(new AboutFragment());
                break;
            case 3:
                replaceFragment(new SettingsFragment());
        }
    }

    private void replaceFragment(Fragment frag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, frag)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else if (findViewById(R.id.weekrooster_fragment) == null)//in the weekroosterFragment
            mNavigationDrawerFragment.onNavigationDrawerItemSelected(0);
        else
            super.onBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_MENU:
                if (!mNavigationDrawerFragment.isDrawerOpen()) {
                    mNavigationDrawerFragment.openDrawer();
                }
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }
}
