package be.pxl.lessenrooster.constants;

/**
 * Created by jeroendebois on 07/08/16.
 */
public class Tracking {

    public interface Screens {
        String ABOUT = "About";
        String SAMENSTELLEN = "Samenstellen";
        String PREFERENCES = "Preferences";
    }

    public interface Categories {
        String ABOUT = "View about";
        String PERSONAL_ROSTER = "Personal roster";

        String PREFERENCES = "View Preferences";
        String VIEW_ROSTER = "View roster";
    }

    public interface Actions {
        String PRESS_LIST_ITEM = "Press list item";
        String ADD_CLASS = "Added class";
        String ADD_OLOD = "Added olod";
        String CHANGE_PAST_WEEKS = "Change Past Weeks preference";
        String CHANGE_FUTURE_DAYS = "Change Future Days preference";
        String MANUAL_REFRESH = "Press manual refresh button";
    }

    public interface Labels {
        String ABOUT_VERSION_ITEM = "Version list item";
        String ABOUT_AUTHOR_ITEM = "Author list item";
        String CHANGE_PAST_WEEKS = "past weeks";
        String CHANGE_FUTURE_DAYS = "future days";
    }
}
