package be.pxl.lessenrooster.io;

/**
 * Interface for asynchronous callbacks
 *
 * @author Jeroen Debois
 */
public interface IAsyncResponse<T> {
    void processFinish(T output);
}
