package be.pxl.lessenrooster.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import be.pxl.lessenrooster.R;


public class NoDataFragment extends Fragment {

    private boolean mGeenLes = false;

    public NoDataFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();

        if (arguments != null) {
            mGeenLes = arguments.getBoolean("GeenLes", false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_no_data, container, false);
        if (mGeenLes) {
            TextView info = (TextView) view.findViewById(R.id.info_message);
            info.setText("Geen les");
        }
        return view;
    }


}
