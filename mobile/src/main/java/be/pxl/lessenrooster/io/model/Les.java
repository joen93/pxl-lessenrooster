package be.pxl.lessenrooster.io.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jeroen on 14/04/15.
 */
public class Les implements Parcelable, Comparable<Les> {
    public static final Creator<Les> CREATOR = new Creator<Les>() {
        public Les createFromParcel(Parcel parcel) {
            return new Les(parcel);
        }

        public Les[] newArray(int size) {
            return new Les[size];
        }
    };
    @SerializedName("van_uur")
    private String mStartTime;
    @SerializedName("tot_uur")
    private String mEndTime;
    private boolean mConflict;
    @SerializedName("olod")
    private String mName;
    @SerializedName("docent")
    private String mDocent;
    @SerializedName("lokaal")
    private String mLokaal;
    @SerializedName("datum2")
    private String mDatum;
    @SerializedName("klas")
    private String mKlas;

    public Les(String startTime, String endTime, boolean conflict,
               String name, String docent, String lokaal, String datum, String klas) {
        this.mStartTime = startTime;
        this.mEndTime = endTime;
        this.mConflict = conflict;
        this.mName = name;
        this.mDocent = docent;
        this.mLokaal = lokaal;
        this.mDatum = datum;
        this.mKlas = klas;
    }

    public Les(Parcel parcel) {
        this.mStartTime = parcel.readString();
        this.mEndTime = parcel.readString();
        this.mConflict = parcel.readByte() != 0;
        this.mName = parcel.readString();
        this.mDocent = parcel.readString();
        this.mLokaal = parcel.readString();
        this.mDatum = parcel.readString();
        this.mKlas = parcel.readString();
    }

    public String getStartTime() {
        return mStartTime;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public boolean getConflict() {
        return mConflict;
    }

    public void setConflict(boolean conflict) {
        this.mConflict = conflict;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getDocent() {
        return mDocent;
    }

    public String getLokaal() {
        return mLokaal;
    }

    public String getDatum() {
        return mDatum;
    }

    public String getKlas() {
        return mKlas;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mStartTime);
        dest.writeString(this.mEndTime);
        dest.writeByte((byte) (this.mConflict ? 1 : 0));

        dest.writeString(this.mName);
        dest.writeString(this.mDocent);
        dest.writeString(this.mLokaal);

        dest.writeString(this.mDatum);
        dest.writeString(this.mKlas);
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass() == this.getClass()) {
            if (!((Les) o).getName().equals(mName))
                return false;
            else if (!((Les) o).getKlas().equals(mKlas))
                return false;
            else if (!((Les) o).getDatum().equals(mDatum))
                return false;
            else if (!(((Les) o).getStartTime().equals(mStartTime) && ((Les) o).getEndTime().equals(mEndTime)))
                return false;

            return true;
        }
        return false;
    }

    @Override
    public int compareTo(@NonNull Les another) {
        int startTime = Integer.parseInt(mStartTime);
        int endTime = Integer.parseInt(mEndTime);
        int otherStartTime = Integer.parseInt(another.getStartTime());
        int otherEndTime = Integer.parseInt(another.getEndTime());

        if ((startTime >= otherStartTime && startTime < otherEndTime)
                || (endTime > otherStartTime && endTime <= otherEndTime)) {
            this.mConflict = true;
            another.setConflict(true);
        }

        if (startTime < otherStartTime)
            return -1;
        if (startTime > otherStartTime)
            return 1;

        //when we got this far, then they're equal
        return 0;
    }
}
