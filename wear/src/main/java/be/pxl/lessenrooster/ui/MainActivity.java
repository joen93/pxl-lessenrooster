package be.pxl.lessenrooster.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.google.gson.reflect.TypeToken;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.io.AsyncResponse;
import be.pxl.lessenrooster.io.Utils;
import be.pxl.lessenrooster.io.model.Les;

public class MainActivity extends FragmentActivity implements AsyncResponse {

    private FragmentAdapter mAdapter;
    private ViewPager mViewPager;
    private Utils mUtils;
    private ArrayList<String> mDates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUtils = new Utils(this);
        mUtils.setCallbacks(this);
        mDates = mUtils.getDates();
        mUtils.getDays();

        mAdapter = new FragmentAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) this.findViewById(R.id.vp_fragment_container);
        mViewPager.setAdapter(mAdapter);

    }

    @Override
    public void processFinish(String output) {
        if (output != null && !output.isEmpty()) {
            TypeToken typeToken = new TypeToken<ArrayList<ArrayList<Les>>>() {
            };
            ArrayList<ArrayList<Les>> days = (ArrayList<ArrayList<Les>>) Utils.jsonToList(typeToken, output);
            String today = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
            int todayIndex = mDates.indexOf(today);
            //remove previous days
            for (int i = 0; i < todayIndex; i++) {
                days.remove(i);
                mDates.remove(i);
            }
            mAdapter.setDays(days);
        }
    }

    //TODO setting to choose amount of fragments (on watch)


    private class FragmentAdapter extends FragmentStatePagerAdapter {
        private ArrayList<ArrayList<Les>> mDays;

        public FragmentAdapter(FragmentManager fm) {
            super(fm);
            mDays = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int i) {
            if (mDays.size() == 0)
                return new NoDataFragment();
            ArrayList<Les> day = mDays.get(i);
            if (day != null && !day.isEmpty()) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("lessen", mDays.get(i));
                LessenroosterFragment lrFrag = new LessenroosterFragment();
                lrFrag.setArguments(bundle);
                return lrFrag;
            }
            NoDataFragment ndf = new NoDataFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("GeenLes", true);
            ndf.setArguments(bundle);
            return ndf;
        }

        @Override
        public int getCount() {
            if (mDays.size() > 1)
                return mDays.size();
            else
                return 1; //for no data fragment
        }

        //Workaround to make the pagerAdapter update
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position >= 0 && position < mDays.size()) {
                SimpleDateFormat sdfIn = new SimpleDateFormat("yyMMdd"); //151221
                SimpleDateFormat sdfOut = new SimpleDateFormat("EEEE dd-MM"); //monday 21-12
                String strDate = mDates.get(position);
                try {
                    Date date = sdfIn.parse(strDate);
                    return sdfOut.format(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return strDate;
                }
            } else
                return "Geen les";
        }

        public synchronized void setDays(ArrayList<ArrayList<Les>> days) {
            this.mDays.clear();
            this.mDays.addAll(days);
            this.notifyDataSetChanged();
        }
    }


}
