package be.pxl.lessenrooster.io.model;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;

import be.pxl.lessenrooster.R;

/**
 * Numberpicker preference
 * Created by jeroen on 8/06/15.
 */
public class PastDaysPickerPreference extends NumberPickerPreference {

    private static final Integer DEFAULT_VALUE = 0;

    public PastDaysPickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        if (restorePersistedValue) {
            //restore existing state
            mCurrentValue = this.getPersistedInt(DEFAULT_VALUE);
        } else {
            //set default state from the XML attribtue
            mCurrentValue = (Integer) defaultValue;
            persistInt(mCurrentValue);
        }
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        this.mNumberPicker = (NumberPicker) view.findViewById(R.id.pref_num_picker);
        mNumberPicker.setMaxValue(4);
        mNumberPicker.setMinValue(0);
        mNumberPicker.setValue(mCurrentValue);
        if (mCurrentValue != null)
            persistInt(mCurrentValue);
        setSummary(mCurrentValue);

    }

    public void setSummary(Integer value) {
        String week = (value == 1) ? getContext().getString(R.string.settings_week) : getContext().getString(R.string.settings_weeks);

        setSummary(String.valueOf(value) + " " + week);
    }
}
