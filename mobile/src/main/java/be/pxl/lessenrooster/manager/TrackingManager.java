package be.pxl.lessenrooster.manager;

/**
 * Created by jeroendebois on 07/08/16.
 */
public interface TrackingManager {

    void trackScreen(String screen);

    void trackEvent(String category, String action, String label);
}
