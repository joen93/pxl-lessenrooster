package be.pxl.lessenrooster.io;


import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.io.model.NavigationItem;


public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {

    private List<NavigationItem> mData;
    private NavigationDrawerCallbacks mNavigationDrawerCallbacks;
    private int mSelectedPosition;

    public NavigationDrawerAdapter(List<NavigationItem> data) {
        mData = data;
    }

    public NavigationDrawerCallbacks getNavigationDrawerCallbacks() {
        return mNavigationDrawerCallbacks;
    }

    public void setNavigationDrawerCallbacks(NavigationDrawerCallbacks navigationDrawerCallbacks) {
        mNavigationDrawerCallbacks = navigationDrawerCallbacks;
    }

    @Override
    public NavigationDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.drawer_row, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NavigationDrawerAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.itemName.setText(mData.get(position).getText());
        viewHolder.icon.setImageDrawable(mData.get(position).getDrawable());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (position == 1) { //samenstellen
                    Utils.isOnline(2, new IAsyncResponse<Boolean>() {
                        @Override
                        public void processFinish(Boolean output) {
                            if (output) {
                                if (mNavigationDrawerCallbacks != null)
                                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position);
                            } else
                                Toast.makeText(v.getContext(), R.string.toast_offline, Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    if (mNavigationDrawerCallbacks != null)
                        mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position);
                }
            }
        });

        Resources resources = viewHolder.itemView.getContext().getResources();
        //selected menu position, change layout accordingly
        if (mSelectedPosition == position) {

            int color = resources.getColor(R.color.myPrimaryColor);
            viewHolder.itemView.setBackgroundColor(
                    resources.getColor(R.color.selected_gray));
            viewHolder.icon.setColorFilter(color);
            viewHolder.itemName.setTextColor(color);
        } else {
            viewHolder.itemView.setBackgroundResource(R.drawable.custom_bg);
            viewHolder.icon.setColorFilter(resources.getColor(R.color.myTextPrimaryColor)); // clear won't work
            viewHolder.itemName.setTextColor(resources.getColor(R.color.myTextPrimaryColor));
        }
    }


    public void selectPosition(int position) {
        int lastPosition = mSelectedPosition;
        mSelectedPosition = position;
        notifyItemChanged(lastPosition);
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemName;
        public ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            itemName = (TextView) itemView.findViewById(R.id.item_name);
            icon = (ImageView) itemView.findViewById(R.id.item_icon);
        }
    }
}