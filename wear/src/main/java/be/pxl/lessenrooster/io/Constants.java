package be.pxl.lessenrooster.io;

/**
 * Created by jeroen on 23/05/15.
 */
public final class Constants {
    public static final String DATES_PATH = "dates";
    public static final String DAYS_PATH = "days";
    public static final String PREFS_LESSENROOSTER_PERIOD = "period";
    public static final String PREFS_FILE_NAME = "LessenroosterPrefs";

    private Constants() {
    }
}
