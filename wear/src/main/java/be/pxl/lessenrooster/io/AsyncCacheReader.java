package be.pxl.lessenrooster.io;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * AsyncTask which reads cache files in the background.
 * Needs to be executed with 1 parameter: (String) filename
 *
 * @author Jeroen Debois
 */
public class AsyncCacheReader extends AsyncTask<String, Void, String> {
    /**
     * Context for I/O
     */
    private Context mContext;
    private AsyncResponse mAsyncResponse;

    public AsyncCacheReader(Context mContext, AsyncResponse asyncResponse) {
        this.mContext = mContext;
        this.mAsyncResponse = asyncResponse;
    }

    /**
     * Read in background
     *
     * @param params String array containing the execution parameters.
     * @return void
     */
    @Override
    protected String doInBackground(String... params) {
        if (params.length != 1) {
            return null;//incorrect params
        }

        String filename = params[0];

        BufferedReader bufferedReader = null;
        StringBuilder builder = new StringBuilder();

        try {
            FileInputStream fileInputStream = mContext.openFileInput(filename);
            bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
            String line = bufferedReader.readLine();
            while (line != null) {
                builder.append(line);
                line = bufferedReader.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        return builder.toString();
    }

    /**
     * After reading the file, hand over the result to the response handler
     *
     * @param s the content of the read file
     */
    @Override
    protected void onPostExecute(String s) {
        mAsyncResponse.processFinish(s);
    }
}
