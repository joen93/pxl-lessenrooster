package be.pxl.lessenrooster.io;

import be.pxl.lessenrooster.io.model.Vak;

/**
 * Created by jeroen on 16/04/15.
 */
public interface VakkenCallbacks {
    void onItemSelected(Vak vak, boolean checked);

    boolean containsVak(Vak vak);
}
