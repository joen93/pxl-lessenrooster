package be.pxl.lessenrooster.io.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jeroen on 15/04/15.
 */
public class Klas {
    @SerializedName("klas")
    private String mName;
    @SerializedName("jaar")
    private String mYear;

    public Klas(String name, String year) {
        this.mName = name;
        this.mYear = year;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getYear() {
        return mYear;
    }

    public void setYear(String year) {
        this.mYear = year;
    }
}
