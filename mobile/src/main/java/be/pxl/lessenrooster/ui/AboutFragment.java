package be.pxl.lessenrooster.ui;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.app.PxlApplication;
import be.pxl.lessenrooster.constants.Tracking.Actions;
import be.pxl.lessenrooster.constants.Tracking.Categories;
import be.pxl.lessenrooster.constants.Tracking.Labels;
import be.pxl.lessenrooster.constants.Tracking.Screens;
import be.pxl.lessenrooster.manager.TrackingManager;

/**
 * A fragment representing a list of Items.
 */
public class AboutFragment extends Fragment {

    @Inject
    TrackingManager mTrackingManager;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AboutFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PxlApplication.get(getContext()).component().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.about_listview);

        String appName = getActivity().getString(getActivity().getApplicationInfo().labelRes);
        String versionName = "";
        try {
            versionName = getActivity().getString(R.string.version_text)
                    + getActivity().getPackageManager().getPackageInfo(
                    getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        final ArrayList<aboutItem> list = new ArrayList<>();
        list.add(new aboutItem(appName, versionName));
        list.add(new aboutItem(getActivity().getString(R.string.developed_by),
                getActivity().getString(R.string.dev_year)));

        ArrayAdapter<aboutItem> adapter = new ArrayAdapter<aboutItem>(
                rootView.getContext(), android.R.layout.simple_list_item_2, R.layout.list_item_about, list) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {

                    LayoutInflater inflater = LayoutInflater.from(getContext());
                    convertView = inflater.inflate(R.layout.list_item_about, null);

                    ViewHolder viewHolder = new ViewHolder();
                    viewHolder.text1 = (TextView) convertView.findViewById(R.id.about_text1);
                    viewHolder.text2 = (TextView) convertView.findViewById(R.id.about_text2);
                    convertView.setTag(viewHolder);
                }

                ViewHolder viewHolder = (ViewHolder) convertView.getTag();
                viewHolder.text1.setText(list.get(position).getText1());
                viewHolder.text2.setText(list.get(position).getText2());
                return convertView;
            }

            class ViewHolder {
                TextView text1;
                TextView text2;
            }
        };


        listView.setAdapter(adapter);
        listView.setClickable(false);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //--Analytics--
                mTrackingManager.trackEvent(Categories.ABOUT,
                        Actions.PRESS_LIST_ITEM,
                        position == 0 ? Labels.ABOUT_VERSION_ITEM : Labels.ABOUT_AUTHOR_ITEM);
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mTrackingManager.trackScreen(Screens.ABOUT);
    }

    private class aboutItem {
        private String text1;
        private String text2;

        public aboutItem(String text1, String text2) {
            this.text1 = text1;
            this.text2 = text2;
        }

        public String getText1() {
            return text1;
        }

        public String getText2() {
            return text2;
        }

    }
}
