package be.pxl.lessenrooster.io;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
