package be.pxl.lessenrooster.io;

/**
 * Interface for asynchronous callbacks
 *
 * @author Jeroen Debois
 */
public interface AsyncResponse {
    void processFinish(String output);
}
