package be.pxl.lessenrooster.io;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.io.model.Vak;

/**
 * Created by jeroen on 15/04/15.
 */
public class VakkenAdapter extends RecyclerView.Adapter<VakkenAdapter.ViewHolder> {
    private static final String TAG = "VakkenAdapter";
    private ArrayList<Vak> mVakken;
    private VakkenCallbacks mVakkenCallbacks;

    public VakkenAdapter(ArrayList<Vak> vakken, VakkenCallbacks selectHandler) {
        this.mVakken = vakken;
        this.mVakkenCallbacks = selectHandler;
    }

    @Override
    public VakkenAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item_vak, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final VakkenAdapter.ViewHolder viewHolder, int i) {
        final Vak vak = mVakken.get(i);
        viewHolder.mName.setText(vak.getOlod());
        viewHolder.mDocent.setText(vak.getDocent());
        viewHolder.mCheckbox.setChecked(mVakkenCallbacks.containsVak(vak));

        viewHolder.mCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVakkenCallbacks.onItemSelected(vak, viewHolder.mCheckbox.isChecked());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mVakken != null ? mVakken.size() : 0;
    }

    public synchronized void setVakken(List<Vak> vakken) {
        this.mVakken.clear();
        this.mVakken.addAll(vakken);
        this.notifyDataSetChanged();
    }

    public synchronized void clearChecks() {
        this.notifyDataSetChanged();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mName;
        private TextView mDocent;
        private CheckBox mCheckbox;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mName = (TextView) itemView.findViewById(R.id.vak_name);
            this.mDocent = (TextView) itemView.findViewById(R.id.vak_docent);
            this.mCheckbox = (CheckBox) itemView.findViewById(R.id.checkBox_volgen);
        }
    }
}

