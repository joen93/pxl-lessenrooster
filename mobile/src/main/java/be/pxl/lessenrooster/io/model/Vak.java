package be.pxl.lessenrooster.io.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jeroen on 15/04/15.
 */
public class Vak implements Parcelable {
    public static final Creator<Vak> CREATOR = new Creator<Vak>() {
        public Vak createFromParcel(Parcel parcel) {
            return new Vak(parcel);
        }

        public Vak[] newArray(int size) {
            return new Vak[size];
        }
    };
    @SerializedName("klas")
    private String mKlas;
    @SerializedName("code_olod")
    private String mCodeOlod;
    @SerializedName("olod")
    private String mOlod;
    @SerializedName("docent")
    private String mDocent;

    public Vak(String klas, String codeOlod, String olod, String docent) {
        this.mKlas = klas;
        this.mCodeOlod = codeOlod;
        this.mOlod = olod;
        this.mDocent = docent;
    }

    public Vak(Parcel parcel) {
        this.mKlas = parcel.readString();
        this.mCodeOlod = parcel.readString();
        this.mOlod = parcel.readString();
        this.mDocent = parcel.readString();
    }

    public String getKlas() {
        return mKlas;
    }

    public void setKlas(String mKlas) {
        this.mKlas = mKlas;
    }

    public String getCodeOlod() {
        return mCodeOlod;
    }

    public void setCodeOlod(String mCodeOlod) {
        this.mCodeOlod = mCodeOlod;
    }

    public String getOlod() {
        return mOlod;
    }

    public void setOlod(String mOlod) {
        this.mOlod = mOlod;
    }

    public String getDocent() {
        return mDocent;
    }

    public void setDocent(String mDocent) {
        this.mDocent = mDocent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mKlas);
        dest.writeString(this.mCodeOlod);
        dest.writeString(this.mOlod);
        dest.writeString(this.mDocent);
    }

    @Override
    public boolean equals(Object o) {
        if (!((Vak) o).getKlas().equals(mKlas))
            return false;
        return ((Vak) o).getCodeOlod().equals(mCodeOlod);
    }

    public String buildApiPath() {
        return "klassen/" + mKlas + "/vakken/" + mCodeOlod;
    }
}
