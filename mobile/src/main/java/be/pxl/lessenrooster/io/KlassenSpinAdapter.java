package be.pxl.lessenrooster.io;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.io.model.Klas;

/**
 * Created by jeroen on 15/04/15.
 */
public class KlassenSpinAdapter extends ArrayAdapter<Klas> {
    private Context mContext;
    private List<Klas> mKlassen;

    public KlassenSpinAdapter(Context context, int resource) {
        super(context, resource);
        this.mContext = context;
        this.mKlassen = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mKlassen.size();
    }

    @Override
    public Klas getItem(int position) {
        return mKlassen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //passive state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.spinner_item_klas, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.klas = (TextView) convertView.findViewById(R.id.klas_text);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.klas.setText(getItem(position).getName());
        return convertView;
    }

    //spinner popped open
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.spinner_item_klas, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.klas = (TextView) convertView.findViewById(R.id.klas_text);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.klas.setText(getItem(position).getName());
        return convertView;
    }


    public synchronized void setKlassen(List<Klas> klassen) {
        this.mKlassen.clear();
        this.mKlassen = klassen;
        this.notifyDataSetChanged();
    }

    private static class ViewHolder {
        TextView klas;
    }
}
