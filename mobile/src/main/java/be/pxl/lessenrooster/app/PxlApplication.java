package be.pxl.lessenrooster.app;

import android.app.Application;
import android.content.Context;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;

import be.pxl.lessenrooster.BuildConfig;
import io.fabric.sdk.android.Fabric;

/**
 * Created by jeroendebois on 07/08/16.
 */
public class PxlApplication extends Application {
    private AppContextComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        final CrashlyticsCore crashlyticsCore = new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build();
        final Crashlytics crashlytics = new Crashlytics.Builder().core(crashlyticsCore).build();
        Fabric.with(this, crashlytics, new Answers());
        mComponent = AppComponent.Initializer.init(this);
        mComponent.inject(this);
    }

    public static PxlApplication get(final Context context) {
        return (PxlApplication) context.getApplicationContext();
    }

    public AppContextComponent component() {
        return mComponent;
    }
}
