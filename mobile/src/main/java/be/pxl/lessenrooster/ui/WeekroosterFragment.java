package be.pxl.lessenrooster.ui;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.gson.reflect.TypeToken;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.app.PxlApplication;
import be.pxl.lessenrooster.constants.Tracking;
import be.pxl.lessenrooster.io.IAsyncResponse;
import be.pxl.lessenrooster.io.Utils;
import be.pxl.lessenrooster.io.model.Les;
import be.pxl.lessenrooster.manager.TrackingManager;

public class WeekroosterFragment extends Fragment implements IAsyncResponse<String> {
    private ViewPager mViewPager;
    private FragmentAdapter mAdapter;
    private Utils mUtils;
    private ArrayList<String> mDates;
    private int mIndexToday = -1;
    private DayChangeListener mChangeListener;
    private ProgressBar mProgressSpinner;
    private NavigationDrawerFragment mDrawerFragment;

    @Inject
    TrackingManager mTrackingManager;

    public WeekroosterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PxlApplication.get(getContext()).component().inject(this);

        setHasOptionsMenu(true);
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mProgressSpinner = (ProgressBar) inflater.inflate(R.layout.action_view_refresh, null);
        mDrawerFragment = (NavigationDrawerFragment) getActivity().getFragmentManager().findFragmentById(R.id.fragment_drawer);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_weekrooster, container, false);

        mAdapter = new FragmentAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) rootView.findViewById(R.id.vp_fragment_container);
        mViewPager.setAdapter(mAdapter);

        this.mUtils = new Utils(getActivity());
        mUtils.setCallbacks(this);
        mUtils.setProgressSpinner(mProgressSpinner);

        this.getData(false);

        return rootView;
    }

    private void getData(boolean forceSync) {
        mUtils.getDays(forceSync);
        mDates = mUtils.getDates();
    }

    @Override
    public void processFinish(String output) {
        TypeToken typeToken = new TypeToken<ArrayList<ArrayList<Les>>>() {
        };
        ArrayList<ArrayList<Les>> days = (ArrayList<ArrayList<Les>>) Utils.jsonToList(typeToken, output);
        mAdapter.setDays(days);
        //change the viewpager to the current day
        String today = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
        mIndexToday = mDates.indexOf(today);
        mViewPager.setCurrentItem(mIndexToday, true);//index of today(or -1) and smooth scroll to that index
        if (mChangeListener != null) {
            mChangeListener.setIndexToday(mIndexToday);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.weekrooster, menu);

        mUtils.setActionRefresh(menu.findItem(R.id.action_refresh));
        //to make sure the viewpager won't get multiple listeners
        if (mChangeListener == null) {
            mChangeListener = new DayChangeListener(menu);
            mChangeListener.setIndexToday(mIndexToday);
            mViewPager.addOnPageChangeListener(mChangeListener);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                this.getData(true);
                //--Analytics--
                mTrackingManager.trackEvent(Tracking.Categories.VIEW_ROSTER,
                        Tracking.Actions.MANUAL_REFRESH,
                        Tracking.Actions.MANUAL_REFRESH);
                return true;
            case R.id.action_to_today:
                mViewPager.setCurrentItem(mIndexToday, true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class FragmentAdapter extends FragmentStatePagerAdapter {
        private ArrayList<ArrayList<Les>> mDays;

        public FragmentAdapter(FragmentManager fm) {
            super(fm);
            mDays = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int i) {
            if (mDays.size() == 0)
                return new NoDataFragment();
            ArrayList<Les> day = mDays.get(i);
            if (day != null && !day.isEmpty()) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("lessen", mDays.get(i));
                LessenroosterFragment lrFrag = new LessenroosterFragment();
                lrFrag.setArguments(bundle);
                return lrFrag;
            }
            NoDataFragment ndf = new NoDataFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("GeenLes", true);
            ndf.setArguments(bundle);
            return ndf;
        }

        @Override
        public int getCount() {
            if (mDays.size() > 1)
                return mDays.size();
            else
                return 1; //for no data fragment
        }

        //Workaround to make the pagerAdapter update
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position >= 0 && position < mDays.size()) {
                SimpleDateFormat sdfIn = new SimpleDateFormat("yyMMdd");
                SimpleDateFormat sdfOut = new SimpleDateFormat("EEEE dd-MM");
                String strDate = mDates.get(position);
                try {
                    Date date = sdfIn.parse(strDate);
                    return sdfOut.format(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return strDate;
                }
            } else
                return "Geen les";
        }

        public synchronized void setDays(ArrayList<ArrayList<Les>> days) {
            this.mDays.clear();
            this.mDays.addAll(days);
            try { //fix for app crashing if the user backs out of the app while it's refreshing
                this.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class DayChangeListener implements ViewPager.OnPageChangeListener {

        private int mIndexToday = -1;
        private Menu mMenu;

        public DayChangeListener(Menu mMenu) {
            this.mMenu = mMenu;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            //nothing
        }

        @Override
        public void onPageSelected(int position) {
            if (mIndexToday >= 0) {
                if (mMenu != null) {
                    MenuItem nowAction = mMenu.findItem(R.id.action_to_today);
                    nowAction.setVisible(position != mIndexToday);
                }
                //---Analytics---
                StringBuilder builder = new StringBuilder("Day: Today");
                //if selected isn't today, append the dif in days i.e. "+2 days"
                if (position != mIndexToday) {
                    int dif = position - mIndexToday; //difference of days of the selected day with today
                    builder.append(' '); //append a whitespace
                    builder.append(dif > 0 ? "+" + dif : dif); //append '+'dif OR only dif if dif is negative
                    builder.append(Math.abs(dif) > 1 ? " days" : " day"); //append "days" OR "day" when the dif is only 1
                }
                mTrackingManager.trackScreen(builder.toString());
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

        public synchronized void setIndexToday(int index) {
            this.mIndexToday = index;
        }
    }
}
