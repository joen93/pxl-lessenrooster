package be.pxl.lessenrooster.io;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.Wearable;

import java.util.concurrent.TimeUnit;

/**
 * Created by jeroen on 23/05/15.
 */
public class WearSyncService extends IntentService
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "WearSyncService";
    private static final long CONNECTION_TIME_OUT_MS = 100;
    private static final String DATA_KEY = "data_key";

    private String mDaysJson;
    private String mDatesJson;
    private GoogleApiClient mGoogleApiClient;

    /**
     * Creates an IntentService.  Invoked by the subclass's constructor.
     * Gives the worker thread the simple class name (important for debugging).
     */
    public WearSyncService() {
        super(WearSyncService.class.getSimpleName());
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            this.mDaysJson = extras.getString(Constants.DAYS_PATH, "");
            this.mDatesJson = extras.getString(Constants.DATES_PATH, "");
        } else {
            Log.e(TAG, "extras is null");
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mGoogleApiClient.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);

        if (mDaysJson != null)
            sendData("/" + Constants.DAYS_PATH, mDaysJson);
        if (mDaysJson != null)
            sendData("/" + Constants.DATES_PATH, mDatesJson);
        mGoogleApiClient.disconnect();
    }

    private void sendData(String path, String json) {
        final PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(path);
        DataMap data = putDataMapRequest.getDataMap();
        data.putString(DATA_KEY, json);
        if (mGoogleApiClient.isConnected()) {
            Wearable.DataApi.putDataItem(
                    mGoogleApiClient, putDataMapRequest.asPutDataRequest()).await();
        } else {
            Log.e(TAG, "Failed to send data item: " + putDataMapRequest
                    + " - Client disconnected from Google Play Services");
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
