package be.pxl.lessenrooster.app;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import javax.inject.Singleton;

import be.pxl.lessenrooster.BuildConfig;
import be.pxl.lessenrooster.R;
import be.pxl.lessenrooster.manager.TrackingManager;
import be.pxl.lessenrooster.manager.impl.GoogleTrackingManager;
import dagger.Module;
import dagger.Provides;

@Module
public final class GoogleModule {

    @Provides
    @Singleton
    Tracker provideTracker(Context context) {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
        analytics.setDryRun(BuildConfig.DEBUG);
        return analytics.newTracker(context.getString(R.string.google_analytics_ua));
    }

    @Provides
    @Singleton
    TrackingManager provideTrackingManager(Tracker tracker) {
        return new GoogleTrackingManager(tracker);
    }

}
