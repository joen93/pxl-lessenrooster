package be.pxl.lessenrooster.io;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * AsyncTask which writes JSON strings to a cache file in the background.
 * Needs to be executed with 2 parameters: (String) filename; (String) JSON
 *
 * @author Jeroen Debois
 */
public class AsyncCacheWriter extends AsyncTask<String, Void, Void> {
    /**
     * Context for I/O
     */
    private Context mContext;

    public AsyncCacheWriter(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * Write in background
     *
     * @param params String array containing the execution parameters.
     * @return void
     */
    @Override
    protected Void doInBackground(String... params) {
        if (params.length != 2) {
            return null;//incorrect params
        }

        String filename = params[0];
        String json = params[1];

        BufferedWriter bufferedWriter = null;

        try {
            FileOutputStream fileOutputStream = mContext.openFileOutput(filename, Context.MODE_PRIVATE);
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
            bufferedWriter.write(json);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        return null;
    }
}
