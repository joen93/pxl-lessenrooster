package be.pxl.lessenrooster.io;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by jeroen on 23/05/15.
 */
public class WearSyncService extends WearableListenerService {
    private static final String TAG = "WearSyncService";
    private static final String DATA_KEY = "data_key";
    private SharedPreferences mPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        mPreferences = getSharedPreferences(Constants.PREFS_FILE_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Called when the wearable device receives data from the companion device
     *
     * @param dataEvents the DataEventBuffer that contains DataEvent with data
     */
    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        super.onDataChanged(dataEvents);
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onDataChanged: " + dataEvents + " for " + getPackageName());
        }

        for (DataEvent event : dataEvents) {
            DataItem item = event.getDataItem();
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                String path = item.getUri().getPath();
                if (path.equals("/" + Constants.DAYS_PATH))
                    updateCache(item, path);
                else if (path.equals("/" + Constants.DATES_PATH))
                    mPreferences.edit().putString(
                            Constants.PREFS_LESSENROOSTER_PERIOD,
                            getDataMapFromDataItem(item).getString(DATA_KEY)).apply();
            } else if (event.getType() == DataEvent.TYPE_DELETED) {
                String path = item.getUri().getPath();
                if (path.equals("/" + Constants.DAYS_PATH) || path.equals("/" + Constants.DATES_PATH))
                    clearCache();
            }
        }
        dataEvents.close();
    }

    /**
     * Update the cache with the new JSON data
     *
     * @param dataItem The dataItem containing the JSON string
     * @param path     The path to determine the type of data
     */
    private void updateCache(DataItem dataItem, String path) {
        DataMap data = getDataMapFromDataItem(dataItem);

        String json = data.getString(DATA_KEY);
        String filename = path.substring(1); //remove the /

        writeCache(filename, json);
    }

    /**
     * Clear the days and dates cache.
     */
    private void clearCache() {
        writeCache(Constants.DAYS_PATH, "");
        mPreferences.edit().putString(Constants.PREFS_LESSENROOSTER_PERIOD, "").apply();
    }

    /**
     * Cache content by writing it to a file
     *
     * @param filename name of the file
     * @param content  content to be cached
     */
    private void writeCache(String filename, String content) {
        BufferedWriter bufferedWriter = null;

        try {
            FileOutputStream fileOutputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
            bufferedWriter.write(content);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    /**
     * Get a DataMap from a DataItem
     *
     * @param dataItem the DataItem that contains the DataMap
     * @return the DataMap that was in the DataItem
     */
    private DataMap getDataMapFromDataItem(DataItem dataItem) {
        DataMapItem mapDataItem = DataMapItem.fromDataItem(dataItem);
        return mapDataItem.getDataMap();
    }
}
