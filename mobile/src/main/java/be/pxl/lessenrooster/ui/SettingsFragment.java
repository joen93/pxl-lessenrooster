package be.pxl.lessenrooster.ui;


import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceCategory;

import com.example.android_support_v4_preferencefragment.PreferenceFragment;

import javax.inject.Inject;

import be.pxl.lessenrooster.R.xml;
import be.pxl.lessenrooster.app.PxlApplication;
import be.pxl.lessenrooster.constants.Tracking.Actions;
import be.pxl.lessenrooster.constants.Tracking.Categories;
import be.pxl.lessenrooster.constants.Tracking.Labels;
import be.pxl.lessenrooster.constants.Tracking.Screens;
import be.pxl.lessenrooster.io.model.NumberPickerPreference;
import be.pxl.lessenrooster.io.model.PastDaysPickerPreference;
import be.pxl.lessenrooster.manager.TrackingManager;

/**
 * A fragment to show the preferences
 */
public class SettingsFragment extends PreferenceFragment implements
        OnSharedPreferenceChangeListener {

    @Inject
    TrackingManager mTrackinManager;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PxlApplication.get(getContext()).component().inject(this);
        addPreferencesFromResource(xml.preferences);
    }

    @Override
    public void onResume() {
        super.onResume();

        //Google analytics
        mTrackinManager.trackScreen(Screens.PREFERENCES);

        //set up a listener whenver a key changes
        getPreferenceScreen().getSharedPreferences().
                registerOnSharedPreferenceChangeListener(this);

        initSummary();
    }

    @Override
    public void onPause() {
        super.onPause();

        //unregister the listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    /**
     * Called when a shared preference is changed, added, or removed. This
     * may be called even if a preference is set to its existing value.
     * <p/>
     * <p>This callback will be run on your main thread.
     *
     * @param sharedPreferences The {@link SharedPreferences} that received
     *                          the change.
     * @param key               The key of the preference that was changed, added, or
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        //update summary
        updatePrefsSummary(sharedPreferences, findPreference(key), false);
    }

    /**
     * Update summary
     *
     * @param sharedPreferences
     * @param pref
     */
    protected void updatePrefsSummary(SharedPreferences sharedPreferences,
                                      Preference pref, boolean initial) {
        if (pref == null)
            return;

        if (pref instanceof NumberPickerPreference) {
            // My NumberPicker Preference
            NumberPickerPreference nrPickerPref = (NumberPickerPreference) pref;
            nrPickerPref.setSummary(nrPickerPref.getValue());

            //--Google analytics--
            if (!initial) {

                mTrackinManager.trackEvent(Categories.PREFERENCES,
                        pref instanceof PastDaysPickerPreference ?
                                Actions.CHANGE_PAST_WEEKS
                                : Actions.CHANGE_FUTURE_DAYS,
                        nrPickerPref.getValue() +
                                (pref instanceof PastDaysPickerPreference ?
                                        Labels.CHANGE_PAST_WEEKS
                                        : Labels.CHANGE_FUTURE_DAYS));
            }
        }
    }

    /**
     * Init summary
     */
    protected void initSummary() {
        int preferenceCount = getPreferenceScreen().getPreferenceCount();
        for (int i = 0; i < preferenceCount; i++) {
            initPrefsSummary(getPreferenceManager().getSharedPreferences(),
                    getPreferenceScreen().getPreference(i));
        }
    }

    /**
     * init single preference summary
     *
     * @param sharedPreferences
     * @param preference
     */
    private void initPrefsSummary(SharedPreferences sharedPreferences, Preference preference) {
        if (preference instanceof PreferenceCategory) {
            PreferenceCategory preferenceCategory = (PreferenceCategory) preference;
            int prefCategoryCount = preferenceCategory.getPreferenceCount();
            for (int i = 0; i < prefCategoryCount; i++) {
                initPrefsSummary(sharedPreferences, preferenceCategory.getPreference(i));
            }
        } else {
            updatePrefsSummary(sharedPreferences, preference, true);

        }
    }
}
