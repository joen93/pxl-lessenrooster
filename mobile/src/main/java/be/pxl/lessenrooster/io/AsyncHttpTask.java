package be.pxl.lessenrooster.io;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

//todo update to use HttpURLConnection http://goo.gl/ikbjnm

/**
 * Class that performs an asynchronous GET.
 * Pass a String containing to the url suffix to execute method for a GET request.
 *
 * @author Jeroen Debois
 */
public class AsyncHttpTask extends AsyncTask<String, Void, String> {
    private static final String BASE_URL = "http://data.pxl.be/roosters/v1/";
    private final ProgressDialog mDialog;
    private final boolean mShowDialog;
    private IAsyncResponse<String> mAsyncResponse = null;

    public AsyncHttpTask(Context context, boolean showDialog, IAsyncResponse<String> asyncResponse) {
        this.mDialog = new ProgressDialog(context);
        this.mShowDialog = showDialog;
        this.mAsyncResponse = asyncResponse;
    }

    @Override
    protected void onPreExecute() {
        if (mShowDialog) {
            mDialog.setTitle("Even geduld aub");
            mDialog.setMessage("Downloading data...");
            mDialog.setIndeterminate(false);
            mDialog.setCancelable(false);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        HttpClient client = new DefaultHttpClient();
        String suffix;
        if (params.length == 1) {
            suffix = params[0];
            return getResult(client, suffix);
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        mAsyncResponse.processFinish(result);
        if (mShowDialog) {
            try {
                mDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String getResult(HttpClient client, String suffix) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        HttpGet httpGet = new HttpGet(BASE_URL + suffix);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                HttpEntity msgEntity = response.getEntity();
                InputStream content = msgEntity.getContent();

                reader = new BufferedReader(new InputStreamReader(content));

                String line = reader.readLine();
                while (line != null) {
                    builder.append(line);
                    line = reader.readLine();
                }
            }
        } catch (ClientProtocolException cpe) {
            cpe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        //return the JSON result
        return builder.toString();
    }

}
