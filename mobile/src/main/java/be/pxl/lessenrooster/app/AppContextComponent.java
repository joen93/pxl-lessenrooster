package be.pxl.lessenrooster.app;

import be.pxl.lessenrooster.ui.AboutFragment;
import be.pxl.lessenrooster.ui.SamenstellenFragment;
import be.pxl.lessenrooster.ui.SettingsFragment;
import be.pxl.lessenrooster.ui.WeekroosterFragment;

/**
 * Created by jeroendebois on 07/08/16.
 */
public interface AppContextComponent {

    void inject(PxlApplication application);

    void inject(AboutFragment aboutFragment);

    void inject(SamenstellenFragment samenstellenFragment);

    void inject(SettingsFragment settingsFragment);

    void inject(WeekroosterFragment weekroosterFragment);
}
