package be.pxl.lessenrooster.io.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;

import be.pxl.lessenrooster.R;

/**
 * Numberpicker preference
 * Created by jeroen on 8/06/15.
 */
public class NumberPickerPreference extends DialogPreference {

    private static final Integer DEFAULT_VALUE = 14;
    NumberPicker mNumberPicker;
    Integer mCurrentValue;

    public NumberPickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDialogLayoutResource(R.layout.numberpicker_dialog);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);

        setDialogIcon(null);
    }

    /**
     * Called when the dialog is dismissed and should be used to save data to
     * the {@link SharedPreferences}.
     *
     * @param positiveResult Whether the positive button was clicked (true), or
     *                       the negative button was clicked or the dialog was canceled (false).
     */
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        //pressed OK
        if (positiveResult) {
            int newValue = mNumberPicker.getValue();
            if (callChangeListener(newValue)) {
                mCurrentValue = newValue;
                persistInt(newValue);
            }

            //setSummary(mCurrentValue);
        }
        super.onDialogClosed(positiveResult);
    }

    /**
     * Implement this to set the initial value of the Preference.
     * <p/>
     * If <var>restorePersistedValue</var> is true, you should restore the
     * Preference value from the {@link SharedPreferences}. If
     * <var>restorePersistedValue</var> is false, you should set the Preference
     * value to defaultValue that is given (and possibly store to SharedPreferences
     * if {@link #shouldPersist()} is true).
     * <p/>
     * This may not always be called. One example is if it should not persist
     * but there is no default value given.
     *
     * @param restorePersistedValue True to restore the persisted value;
     *                              false to use the given <var>defaultValue</var>.
     * @param defaultValue          The default value for this Preference. Only use this
     */
    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        if (restorePersistedValue) {
            //restore existing state
            mCurrentValue = this.getPersistedInt(DEFAULT_VALUE);
        } else {
            //set default state from the XML attribtue
            mCurrentValue = (Integer) defaultValue;
            persistInt(mCurrentValue);
        }
    }

    /**
     * Sets the default value for this Preference, which will be set either if
     * persistence is off or persistence is on and the preference is not found
     * in the persistent storage.
     *
     * @param defaultValue The default value.
     */
    @Override
    public void setDefaultValue(Object defaultValue) {
        super.setDefaultValue(defaultValue);
        mCurrentValue = defaultValue instanceof Integer ? (Integer) defaultValue
                : Integer.parseInt(defaultValue.toString());
    }

    /**
     * Called when a Preference is being inflated and the default value
     * attribute needs to be read. Since different Preference types have
     * different value types, the subclass should get and return the default
     * value which will be its value type.
     * <p/>
     * For example, if the value type is String, the body of the method would
     * proxy to {@link TypedArray#getString(int)}.
     *
     * @param a     The set of attributes.
     * @param index The index of the default value attribute.
     * @return The default value of this preference type.
     */
    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInteger(index, DEFAULT_VALUE);
    }

    /**
     * Binds views in the content View of the dialog to data.
     * <p/>
     * Make sure to call through to the superclass implementation.
     *
     * @param view The content View of the dialog, if it is custom.
     */
    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        this.mNumberPicker = (NumberPicker) view.findViewById(R.id.pref_num_picker);
        mNumberPicker.setMaxValue(31);
        mNumberPicker.setMinValue(1);
        mNumberPicker.setValue(mCurrentValue);
        if (mCurrentValue != null)
            persistInt(mCurrentValue);
        setSummary(mCurrentValue);

    }

    public void setSummary(Integer value) {
        String day = (value == 1) ? getContext().getString(R.string.settings_day) : getContext().getString(R.string.settings_days);

        setSummary(String.valueOf(value) + " " + day);
    }


    public Integer getValue() {
        return mCurrentValue;
    }


    @Override
    protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        //check wheter this Preference is persistent (continually saved)
        if (isPersistent()) {
            //no need to save instance state because it's persistent
            //use superclass state
            return superState;
        }

        //create instance of custom BaseSavedState
        final SavedState savedState = new SavedState(superState);
        // set the state's value with class memeber that holds current setting value
        savedState.value = mCurrentValue;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        //check wheter the state was saved in onSaveInstanceState
        if (state == null || !state.getClass().equals(SavedState.class)) {
            //didn't save the state in onSaveInstanceState, so call superclass
            super.onRestoreInstanceState(state);
            return;
        }

        //cast state to custom BaseSavedState and pass to superclass
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());

        //set this preference's widget to reflect the restored state
        mNumberPicker.setValue(savedState.value);
    }

    private static class SavedState extends BaseSavedState {

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
        /**
         * Member that holds the setting's value
         */
        int value;

        public SavedState(Parcel source) {
            super(source);
            // get the currect preference's value
            value = source.readInt();
        }

        public SavedState(Parcelable superState) {
            super(superState);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            //write the preference's value
            dest.writeInt(value);
        }
    }
}
